import liquibase.exception.LiquibaseException;

import java.io.IOException;

public class Xtoy {

    public static void main(String[] args) throws IOException, LiquibaseException {
        Converter converter = new Converter();

        if (args.length == 2) {
            converter.convert(args[0], args[1]);
        } else {
            System.err.println("Error: Invalid parameters.");
            System.exit(0);
        }

    }

}
