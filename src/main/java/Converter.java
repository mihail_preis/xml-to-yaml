import liquibase.changelog.ChangeLogParameters;
import liquibase.changelog.DatabaseChangeLog;
import liquibase.database.core.PostgresDatabase;
import liquibase.exception.ChangeLogParseException;
import liquibase.exception.LiquibaseException;
import liquibase.parser.ChangeLogParser;
import liquibase.parser.ChangeLogParserFactory;
import liquibase.resource.FileSystemResourceAccessor;
import liquibase.serializer.ChangeLogSerializer;
import liquibase.serializer.ChangeLogSerializerFactory;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class Converter {

    void convert(String inputFile, String outputFile) {
        try {

            final PostgresDatabase database = new PostgresDatabase();
            ChangeLogParameters parameters = new ChangeLogParameters(database);
            final FileSystemResourceAccessor resourceAccessor = new FileSystemResourceAccessor();
            final ChangeLogParser parser = ChangeLogParserFactory.getInstance().getParser(".xml", resourceAccessor);
            final DatabaseChangeLog changeLog = parser.parse(inputFile, parameters, resourceAccessor);
            final FileOutputStream outputStream = new FileOutputStream(outputFile);
            final ChangeLogSerializer serializer = ChangeLogSerializerFactory.getInstance().getSerializer(".yaml");
            serializer.write(changeLog.getChangeSets(), outputStream);
            System.out.println("Convert complete!");

        } catch (ChangeLogParseException e) {
            System.err.println("Error: File not open for input.");
            System.exit(0);
        } catch (LiquibaseException e) {
            System.err.println("Error: Invalid file.");
            System.exit(0);
        } catch (FileNotFoundException e) {
            System.err.println("Error: File not found.");
            System.exit(0);
        } catch (IOException e) {
            System.err.println("Error: File access denied.");
            System.exit(0);
        }
    }

}
