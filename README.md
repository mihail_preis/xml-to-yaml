Xtoy (XML to Yaml)
=============================
Database change log file converter

INSTALLATION AND USE
------------
1. Download xtoy-*.zip or xtoy-*.tar
2. Unpack in any place and 
3. Run xtoy.bat (if you use Windows) or xtoy.sh (if you use Linux) in terminal with parameters: 
  path on input [.xml] and output [.yaml] file

        > cd XTOY_PATH/bin
        > xtoy.bat C:/temp/db.changelog.xml C:/temp/db.changelog.yaml
        
        or
        
        $ cd XTOY_PATH/bin
        $ sh xtoy.sh /tmp/db.changelog.xml /tmp/db.changelog.yaml

## License
  Xtoy is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).
